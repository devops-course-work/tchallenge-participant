# Stage 1: Compile and Build angular codebase
FROM node:14 as build

WORKDIR /app

COPY /source/package*.json ./

RUN npm install

COPY /source/ .

COPY /env/environment.ts ./src/environments/environment.ts

RUN npm run build --prod


# Stage 2: Serve app with nginx server
FROM nginx

# Copy the build output to replace the default nginx contents.
COPY --from=build /app/dist /usr/share/nginx/html
COPY ./http-static.conf /etc/nginx/conf.d/default.conf

EXPOSE 4200

CMD ["nginx", "-g", "daemon off;"]
